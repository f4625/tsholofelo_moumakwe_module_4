import 'package:flutter/material.dart';
import 'package:flutter_application_1/SignUp.dart';
import 'package:flutter_application_1/dashboard.dart';

void main() => runApp(const feature1());

class feature1 extends StatelessWidget {
  const feature1({Key? key}) : super(key: key);

  static const String _title = 'Feature 1';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const MyStatefulWidget(),
        backgroundColor: Colors.lime,
        floatingActionButton: FloatingActionButton(onPressed: () {
          Navigator.pop(
            context,
            MaterialPageRoute(builder: (context) => const dashboard()),
          );
        }), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  '',
                  style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  '',
                  style: TextStyle(fontSize: 20),
                )),
          ],
        ));
  }
}
